﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoosePanel : MonoBehaviour
{
    [SerializeField]
    private Text _resultText;

    // [SerializeField]
    // private GameObject _loosePanel;

    private void Start()
    {
        gameObject.SetActive(false);
    }

    public void Show(int result)
    {
        gameObject.SetActive(true);
        _resultText.text = result.ToString();
    }

    public void Restart()
    {
        SceneManager.LoadScene("SampleScene");
        gameObject.SetActive(false);
    }

    public void OpenApp()
    {
        Application.OpenURL("https://play.google.com/store/apps/details?id=ru.vtb.invest");
    }
}