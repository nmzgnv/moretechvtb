﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;


public class GameManager : MonoBehaviour
{
    public static int[] Resources;
    public static readonly int MaxResource = 100;
    public static readonly int MinResource = 0;

    // UI
    public TMP_Text actionQuote;
    public TMP_Text characterDialogue;
    public TMP_Text moneyText;

    [SerializeField]
    private LoosePanel _loosePanel;

    // Card
    [SerializeField]
    private GameObject cardGameObject;

    [SerializeField]
    private CardController mainCardController;

    private UIManager _uiManager;

    [SerializeField]
    private SpriteRenderer cardSpriteRenderer;

    [SerializeField]
    private Card currentCard;

    [SerializeField]
    private Vector2 defaultCardPosition;

    [SerializeField]
    private float _rotatingCoefficient;

    private string leftQuote;
    private string rightQuote;
    private Vector3 cardRotation;

    // Movement
    [SerializeField]
    private float fMovingSpeed;

    [SerializeField]
    private float fSwipeMargin;

    [SerializeField]
    private float fSwipeTrigger;

    [SerializeField]
    private Color textColor;

    private Vector3 pos;
    private ResourceManager _resourceManager;
    private int _money = 1000;
    private bool hasLost = false;

    private void CheckLoose()
    {
        foreach (var resource in Resources)
        {
            if (resource <= 0)
            {
                _loosePanel.Show(_money);
                hasLost = true;
            }
        }
    }

    private void Start()
    {
        hasLost = false;
        Resources = new[] {50, 50, 50, 50};
        _resourceManager = GetComponent<ResourceManager>();
        _uiManager = GetComponent<UIManager>();
        LoadCard(currentCard);
    }

    private void AddSomeMoney()
    {
        _money += Random.Range(70, 130);
        moneyText.text = $"Накоплено: {_money} ВТБ";
    }

    private void FixedUpdate()
    {
        if (hasLost)
            return;
        
        var cardPositionX = cardGameObject.transform.position.x;
        // Movement
        if (Input.GetMouseButton(0) && mainCardController.isMouseOver)
        {
            var newPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            newPosition.z = 0;
            cardGameObject.transform.position = newPosition;
        }
        else
        {
            cardGameObject.transform.position =
                Vector2.MoveTowards(cardGameObject.transform.position, defaultCardPosition, fMovingSpeed);
            cardGameObject.transform.eulerAngles = Vector3.zero;
        }

        // Rotating
        cardGameObject.transform.eulerAngles = new Vector3(0, 0, cardPositionX * _rotatingCoefficient);
        textColor.a = Mathf.Min(Math.Abs(cardPositionX / 2), 1);
        actionQuote.color = textColor;
        if (cardPositionX > fSwipeMargin)
        {
            actionQuote.text = rightQuote;
            // cardSpriteRenderer.color = Color.blue;
            UIManager.ShowImpact(currentCard.rightImpact);

            if (!Input.GetMouseButton(0) && cardPositionX > fSwipeTrigger)
            {
                currentCard.Right();
                AddSomeMoney();
                SetNewCard();
            }
        }
        else if (cardPositionX < -fSwipeMargin)
        {
            actionQuote.text = leftQuote;
            // cardSpriteRenderer.color = Color.red;
            UIManager.ShowImpact(currentCard.leftImpact);

            if (!Input.GetMouseButton(0) && cardPositionX < fSwipeTrigger)
            {
                currentCard.Left();
                AddSomeMoney();
                SetNewCard();
            }
        }
        else
        {
            UIManager.ShowImpact(new[] {0, 0, 0, 0});
            cardSpriteRenderer.color = Color.white;
        }

        CheckLoose();
    }


    public void LoadCard(Card card)
    {
        cardSpriteRenderer.sprite = card.sprite;
        leftQuote = card.leftQuote;
        rightQuote = card.rightQuote;
        characterDialogue.text = card.dialogue;
        currentCard = card;
    }

    public void SetNewCard()
    {
        var newCardindex = Random.Range(0, _resourceManager.cards.Length);
        LoadCard(_resourceManager.cards[newCardindex]);
    }
}