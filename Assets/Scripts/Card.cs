﻿using UnityEngine;

[CreateAssetMenu]
public class Card : ScriptableObject
{
    public Sprite sprite;
    public string leftQuote;
    public string rightQuote;
    public string dialogue;

    // Imact values
    public int[] rightImpact = new[] {0, 0, 0, 0};
    public int[] leftImpact = new[] {0, 0, 0, 0};

    public void Left()
    {
        Debug.Log($"card swiped left");
        for (int i = 0; i < GameManager.Resources.Length; i++)
            GameManager.Resources[i] += leftImpact[i];
    }

    public void Right()
    {
        Debug.Log($"card swiped right");
        for (int i = 0; i < GameManager.Resources.Length; i++)
            GameManager.Resources[i] += rightImpact[i];
    }
}