﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using UnityEngine;
using Random = UnityEngine.Random;


public class ActionSystem: MonoBehaviour
{
    private const string actionType = "action";
    private const string chainedActionType = "chainedAction";
    private int standardSeries;
    private readonly int maxSeries;
    private Dictionary<int, Action> standardActions;
    private Dictionary<int, Action> questActions;
    private Dictionary<int, Action> allActions;
    public string path;
    private Action currentAction;

    ActionSystem(Dictionary<int, Action> standardActions, Dictionary<int, Action> questActions, int maxSeries)
    {

        this.standardActions = standardActions;
        this.questActions = questActions;
        this.standardSeries = 0;
        this.maxSeries = maxSeries;
        this.allActions = new Dictionary<int, Action>();
        this.standardActions.ToList().ForEach(x => this.allActions.Add(x.Key, x.Value));
        this.questActions.ToList().ForEach(x => this.allActions.Add(x.Key, x.Value));
    }

    ActionSystem(int maxSeries) : this(new Dictionary<int, Action>(), new Dictionary<int, Action>(), maxSeries)
    {

    }

    ActionSystem(): this(100)
    {

    }

    private void Start()
    {
        fromJson(path);
        Debug.Log("test");

    }

    public void fromJson(string file)
    {
//        string json = @"{
//    ""actions"": {
//        ""1"": {
//            ""type"": ""action"",
//            ""desc"": ""Умный город хайпует"",
//            ""yesText"": ""Вложиться"",
//            ""noText"": ""Игнорировать"",
//            ""sprite"": ""Assets/Sprites/Images/3D Model_Monochromatic.png"",
//            ""statChange"": {
//                ""stock"": 10,
//                ""longTermBonds"": 10,
//                ""shortTermBonds"": 10,
//                ""gold"": 10
//            },
//            ""weight"": 1,
//            ""yesAction"": null,
//            ""noAction"": null
//        }
//    }
//}
//";
        string json = File.ReadAllText(file);
        ActionsDeserialise actionsDeserialise = JsonConvert.DeserializeObject<ActionsDeserialise>(json);
        foreach(KeyValuePair<int, CustomActionDeserialise> entry in actionsDeserialise.actions)
        {
            if (allActions.ContainsKey(entry.Key))
            {
                continue;
            }
            recursiveDeserialise(actionsDeserialise, entry.Key, entry.Value);
        }
        Debug.Log(allActions.Count);
        Debug.Log(standardActions.Count);
        Debug.Log(questActions.Count);

    }

    public void recursiveDeserialise(ActionsDeserialise actionsDeserialise, int key, CustomActionDeserialise action)
    {

        Action desirialisedAction;
        switch (action.type)
        {
            case chainedActionType:

            case actionType:
            default:
                desirialisedAction = new Action(action);
                if (string.IsNullOrEmpty(action.parent))
                {
                    desirialisedAction = new Action(action);
                } else
                {
                    int parentKey = int.Parse(action.parent);
                    ((ChainedAction)allActions[parentKey]).Add(desirialisedAction);
                }
                break;
            
        }
        allActions.Add(key, desirialisedAction);
        if (action.yesAction != null)
        {
            int yesActionKey = int.Parse(action.yesAction);
            Action yesAction;
            if (!allActions.TryGetValue(yesActionKey, out yesAction))
            {
                recursiveDeserialise(actionsDeserialise, yesActionKey, actionsDeserialise.actions[yesActionKey]);
                yesAction = allActions[yesActionKey];
            }
            desirialisedAction.SetYesAction(yesAction);

        }
        if (action.noAction != null)
        {
            int noActionnKey = int.Parse(action.yesAction);
            Action noAction;
            if (!allActions.TryGetValue(noActionnKey, out noAction))
            {
                recursiveDeserialise(actionsDeserialise, noActionnKey, actionsDeserialise.actions[noActionnKey]);
                noAction = allActions[noActionnKey];
            }
            desirialisedAction.SetYesAction(noAction);

        }

    }

    public Action Next(bool yes)
    {
        if (currentAction != null)
        {
            Action nextAction = currentAction.Next(yes);
            if (nextAction != null)
            {
                currentAction = nextAction;
            }
        } else
        {
            Action quest = ReplaceOnQuest();
            if (quest == null)
            {
                standardSeries++;
                currentAction = SelectStandard();
            } else
            {
                currentAction = quest;
            }
        }

        return currentAction;
    }

    private Action ReplaceOnQuest()
    {
        if (standardSeries < GetRandom(maxSeries))
        {
            return SelectRandom(questActions.Values);
        }
        return null;
    }

    private Action SelectStandard()
    {
        return standardActions[GetRandom(standardActions.Count)];
    }

    private Action SelectRandom(Dictionary<int, Action>.ValueCollection actions)
    {
        int sum = 0;
        foreach(Action action in actions)
        {
            sum += action.GetWeight();
        }
        int selected = GetRandom(sum);
        sum = 0;
        foreach (Action action in actions)
        {
            sum += action.GetWeight();
            if (selected < sum)
            {
                return action;
            }
        }
        return null;
    }

    private int GetRandom(int maxInt)
    {
        return Random.Range(0, maxInt);
    }

    
}
