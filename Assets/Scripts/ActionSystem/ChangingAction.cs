﻿using System;
public abstract class ChangingAction: Action
{

    private int changeState = 0;

    public int GetChangeState()
    {
        return changeState;
    }

    public new Action Next(bool yes)
    {
        Change(yes);
        return base.Next(yes);
    }

    public abstract void Change(bool yes);


       
}
