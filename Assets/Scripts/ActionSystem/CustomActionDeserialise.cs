﻿using System;
using Newtonsoft.Json;
using UnityEngine;
public class CustomActionDeserialise
{
    [JsonProperty("type")]
    public string type { get; set; }
    [JsonProperty("type")]
    public string parent { get; set; }
    [JsonProperty("desc")]
    public string desc { get; set; }
    [JsonProperty("yesText")]
    public string yesText { get; set; }
    [JsonProperty("noText")]
    public string noText { get; set; }
    [JsonProperty("sprite")]
    public string sprite { get; set; }
    [JsonProperty("statChange")]
    public StatChange statChange { get; set; }
    [JsonProperty("weight")]
    public int weight { get; set; }
    [JsonProperty("yesAction")]
    public string yesAction { get; set; }
    [JsonProperty("noAction")]
    public string noAction { get; set; }

}
