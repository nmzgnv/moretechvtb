﻿using System;
using Newtonsoft.Json;

public class StatChange
{
    [JsonProperty("stock")]
    private readonly int stock;
    [JsonProperty("longTermBonds")]
    private readonly int longTermBonds;
    [JsonProperty("shortTermBonds")]
    private readonly int shortTermBonds;
    [JsonProperty("gold")]
    private readonly int gold;

    public StatChange()
    {
        this.stock = 0;
        this.longTermBonds = 0;
        this.shortTermBonds = 0;
        this.gold = 0;
    }

    public StatChange(int stock, int longTermBonds, int shortTermBonds, int gold)
    {
        this.stock = stock;
        this.longTermBonds = longTermBonds;
        this.shortTermBonds = shortTermBonds;
        this.gold = gold;
    }

    public StatChange(int[] impact)
    {
        this.stock = impact[0];
        this.longTermBonds = impact[1];
        this.shortTermBonds = impact[2];
        this.gold = impact[3];
    }

    public StatChange SetStock(int stock)
    {
        return ChangeField(0, stock);
    }

    public StatChange SetLongTermBonds(int longTermBonds)
    {
        return ChangeField(1, longTermBonds);
    }

    public StatChange SetShortTermBonds(int shortTermBonds)
    {
        return ChangeField(2, shortTermBonds);
    }

    public StatChange SetGold(int gold)
    {
        return ChangeField(3, gold);
    }

    private StatChange ChangeField(int index, int value)
    {
        int[] impact = GetImpact();
        impact[index] = value;
        return new StatChange(impact);
    }

    public int[] GetImpact()
    {
        return new int[] { stock, longTermBonds, shortTermBonds, gold };
    }
    
}
