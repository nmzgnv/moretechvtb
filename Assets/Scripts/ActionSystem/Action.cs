﻿using System;
using UnityEngine;
public class Action
{
    internal string desc;
    internal string yesText;
    internal string noText;
    internal Sprite sprite;
    internal StatChange impact;
    Action yesAction;
    Action noAction;
    internal int weight;

    public Action(string desc, string yesText, string noText, Sprite sprite, StatChange impact)
    {
        this.desc = desc;
        this.yesText = yesText;
        this.noText = noText;
        this.sprite = sprite;
        this.impact = impact;
        weight = 1;
    }

    public Action() : this("", "", "", null, new StatChange())
    {
    }

    public Action(CustomActionDeserialise action) : this(action.desc, action.yesText, action.noText, null, action.statChange)
    {
        if (action.sprite != null)
        {
            this.sprite = Resources.Load<Sprite>(action.sprite);
        }
    }

    public string GetDescription()
    {
        return desc;
    }

    public string GetYesText()
    {
        return yesText;
    }

    public string GetNoText()
    {
        return noText;
    }

    public Sprite GetSprite()
    {
        return sprite;
    }

    public StatChange GetStatChange()
    {
        return impact;
    }

    public int GetWeight()
    {
        return weight;
    }

    public void SetYesAction(Action action)
    {
        yesAction = action;
    }

    public void SetNoAction(Action action)
    {
        noAction = action;
    }

    public Action Next(bool yes)
    {
        if (yes)
        {
            return yesAction;
        }
        return noAction;
    }
}
