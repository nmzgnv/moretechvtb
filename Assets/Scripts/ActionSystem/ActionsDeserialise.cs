﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

public class ActionsDeserialise
{
    [JsonProperty("actions")]
    public Dictionary<int, CustomActionDeserialise> actions { get; set; }
}
