﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class ChainedAction: Action
{
    private List<Action> actions;
    private int actionPos;

    public ChainedAction(List<Action> actions)
    {
        this.actionPos = -1;
        this.actions = new List<Action>(actions);
        if (this.actions.Count > 0)
        {
            SetFields(this.actions[this.actionPos]);
        }

    }

    private void SetFields(string desc, string yes, string no, Sprite sprite, StatChange impact)
    {
        this.desc = desc;
        this.yesText = yes;
        this.noText = no;
        this.sprite = sprite;
        this.impact = impact;
    }

    public new int GetWeight()
    {
        return GetCurrentAction().GetWeight();
    }

    private void SetFields(Action action)
    {
        SetFields(
            action.GetDescription(),
            action.GetYesText(),
            action.GetNoText(),
            action.GetSprite(),
            action.GetStatChange()
            );
    }

    public void Add(Action action)
    {
        actions.Add(action);
        if (IsChainEnded())
        {
            actionPos++;
            SetFields(action);
        } 
    }

    public new Action Next(bool yes)
    {
        if (IsChainEnded())
        {
            return null;
        }

        NextAction(yes);
        return this;
    }

    private Action GetAction(int index)
    {
        return actions[index];
    }

    private Action GetCurrentAction()
    {
        return GetAction(actionPos);
    }

    private void NextAction(bool yes)
    {
        if (IsChainEnded())
        {
            return;
        }

        Action next = GetCurrentAction().Next(yes);
        if (next == null)
        {
            actionPos++;
        }
        else
        {
            ReplaceAction(actionPos, next);
        }

        SetFields(GetCurrentAction());

    }

    private void ReplaceAction(int index, Action action)
    {
        actions[index] = action;
    }

    private bool IsChainEnded()
    {
        return actionPos + 1 == actions.Count;
    }

}
