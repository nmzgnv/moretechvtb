﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    // Icons impact
    [SerializeField]
    private GameObject[] tempImpacts;

    public static GameObject[] resourceImpacts;

    // Value icons    
    public Image[] resourceValues;

    private void Awake()
    {
        resourceImpacts = tempImpacts;
    }

    private void Update()
    {
        for (int i = 0; i < GameManager.Resources.Length; i++)
        {
            resourceValues[i].fillAmount = ((float) GameManager.Resources[i] / GameManager.MaxResource);
        }
    }

    public static void ShowImpact(int[] impactValues)
    {
        for (int i = 0; i < impactValues.Length; i++)
        {
            resourceImpacts[i].SetActive(impactValues[i] != 0);
        }
    }
}